% %% Load data
length=[100 259 100 249];
maxsize =[303 303 303 303];
factor = 1/3;
for dataseti = 1

loadname = strcat('train_FD00',num2str(dataseti),'.txt');
load(strcat('train_FD00',num2str(dataseti),'.txt'));
load(strcat('test_FD00',num2str(dataseti),'.txt'));
load(strcat('RUL_FD00',num2str(dataseti),'.txt'));
% 
% load train_FD002.txt
% load test_FD002.txt
% load RUL_FD002.txt
% 
% load train_FD003.txt
% load test_FD003.txt
% load RUL_FD003.txt
% 
% load train_FD004.txt
% load test_FD004.txt
% load RUL_FD004.txt
% 
% save train_FD001
% save test_FD001
% save RUL_FD001
% 
% save train_FD002
% save test_FD002
% save RUL_FD002
% 
% save train_FD003
% save test_FD003
% save RUL_FD003
% 
% save train_FD004
% save test_FD004
% save RUL_FD004


%%
close all

%%
aa=test_FD001(1:192,3:26)';
for i=1:24
% i=1;
    bb(i,:)=zscore(aa(i,:));
end
% 
% figure,imshow(bb,[]);

for ii=1:length(i)
    a(ii)=max(find(test_FD001(:,1)==ii));
    if ii>1
       b(ii)=a(ii)-a(ii-1);
    end
end
b(1)=a(1);

diff=max(test_FD001)-min(test_FD001);

%%
sampleno=length(i);
X=zeros(maxsize(i),24,length(i)); % time,settings and measurement, unit number

for i=1:sampleno
    if i==1
        X(1:a(i),1:24,i)=test_FD001(1:a(i),3:26);
    else
        X(1:b(i),1:24,i)=test_FD001(a(i-1)+1:a(i),3:26);
    end
    %[m,n]=size(X);
    %X_normc=X./repmat(max(X),[m,1]);
%     figure,st = controlchart(X_normc,'chart',{'xbar' 'r'});
end
%% PCA data
        disp('start pca')
        Xpca =test_FD001(:,3:26);
        %normmax=max(abs(Xpca));
        for i = 1:size(Xpca,2)
            Xnorm(:,i) = normc(Xpca(:,i));
        end
        [coeff,score,latent,tsquared,explained,mu] = pca(Xnorm);
        
        % use 2 dimensionals.
Xpca2 = zeros(maxsize(i),2,length(i));
clear normtemp

for i=1:sampleno
       i
        %b(i)
        pcatemp =X(factor*b(i):b(i),:,i);
        %normmax=max(abs(Xpca));
        for j = 1:size(pcatemp,2)
            normtemp(:,j) = normc(pcatemp(:,j));
        end
        
        [coeff,score,latent,tsquared,explained,mu]= pca(normtemp);
        size(Xpca2(factor*b(i):b(i),: ,i))
        size(score(:,1:2))
        Xpca2(factor*b(i):b(i),: ,i)= score(:,1:2);
        clear normtemp
        %ppca(i,:)=tempm(:);

    %[m,n]=size(X);
    %X_normc=X./repmat(max(X),[m,1]);
%     figure,st = controlchart(X_normc,'chart',{'xbar' 'r'});
end

%%
level = 5;
%vars_std = zeros(24,level,10)
for j=1:sampleno
    for i = 1:24
%         size(X(1:b(j),i,j)')
%         figure
%         plot(X(1:b(j),i,j)')
;
        [C,S] = wavedec(X(1:b(j),i,j)',level,'haar');
%         size(C);
%         figure
%         plot(C(1:S(1)));
%         C(1:S(1) );
        
        for si= 1:(size(S,2)-1)
            start=sum(S(1:si))-S(si)+1;
            sum(S(1:si));
            %C(start:sum(S(1:si)))
            vars_std(i,si,j) = sqrt(var(C(start:sum(S(1:si)))));
        end
        %var
    end
end

% for i = 1:24
%     for j = 1:10
%         vars_std_n(i,:,j) = normc(vars_std(i,:,j));
%     end
% end
% 
%     temp = vars_std_n(:,:,i)'
%     [coeff,score,latent,tsquared,explained,mu] = pca(vars_std_n(:,:,i)')
%% neural network
for i = 1:sampleno
    temp = vars_std(:,:,i)';
    p(i,:) = temp(:);
end

t = RUL_FD001;

p_train = p(1:80,:);
t_train = t(1:80,:);
p_test=p(81:100,:);
t_test=t(81:100,:);
%% cross validation
desired_spread=[];
mse_max=10e20;
desired_input=[];
desired_output=[];
result_perfp=[];
indices = crossvalind('Kfold',size(p_train,1),4);
h=waitbar(0,'Looking for optimization parameters....')
k=1;
for i = 1:4
    perfp=[];
    disp(['This is No ',num2str(i),' Cross validation'])
    test = (indices == i); train = ~test;
    p_cv_train=p_train(train,:);
    t_cv_train=t_train(train,:);
    p_cv_test=p_train(test,:);
    t_cv_test=t_train(test,:);
    p_cv_train=p_cv_train';
    t_cv_train=t_cv_train';
    p_cv_test= p_cv_test';
    t_cv_test= t_cv_test';
    [p_cv_train,minp,maxp,t_cv_train,mint,maxt]=premnmx(p_cv_train,t_cv_train);
    p_cv_test=tramnmx(p_cv_test,minp,maxp);
    for spread=0.1:0.1:2;
        net=newgrnn(p_cv_train,t_cv_train,spread);
        waitbar(k/80,h);
        disp(['spread value:', num2str(spread)]);
        test_Out=sim(net,p_cv_test);
        test_Out=postmnmx(test_Out,mint,maxt);
        error=t_cv_test-test_Out;
        disp(['current mse is',num2str(mse(error))])
        perfp=[perfp mse(error)];
        if mse(error)<mse_max
            mse_max=mse(error);
            desired_spread=spread;
            desired_input=p_cv_train;
            desired_output=t_cv_train;
        end
        k=k+1;
    end
    result_perfp(i,:)=perfp;
end;
close(h)
disp(['bset spread',num2str(desired_spread)])
disp(['best input'])
desired_input
disp(['best output'])
desired_output
%% set up GRNN netwrok
net=newgrnn(desired_input,desired_output,desired_spread);
p_test=p_test';
p_test=tramnmx(p_test,minp,maxp);
grnn_prediction_result=sim(net,p_test);
grnn_prediction_result=postmnmx(grnn_prediction_result,mint,maxt);
grnn_error=t_test-grnn_prediction_result';
disp(['GRNN nueral network prediction error for RUL ',num2str(abs(grnn_error'))])
save best desired_input desired_output p_test t_test grnn_error mint maxt
dip('mean error')
mean(abs(grnn_error))
save grnn_error
save explained

end
%web browser http://www.ilovematlab.cn/thread-65171-1-1.html
%%
%
% <html>
% <table align="center" >	<tr>		<td align="center"><font size="2">版权所有：</font><a
% href="http://www.ilovematlab.cn/">Matlab中文论坛</a>&nbsp;&nbsp; <script
% src="http://s3.cnzz.com/stat.php?id=971931&web_id=971931&show=pic" language="JavaScript" ></script>&nbsp;</td>	</tr></table>
% </html>
%
%% Feature calculations
% feature=zeros(21,6);
% for sensororder=1:21
% 
%     series=X(1:b(1),sensororder,1);
%     
% %     %% fft
% %     figure,
% %     spectrogram(series',5);colorbar
% %     subplot(2,1,1),plot(series);grid on;
% %     subplot(2,1,2),plot(abs(fft(series,100)));grid on;
% 
%     %% wavelet
%     len = length(series);
%     figure,
%     [cw1,sc] = cwt(series,1:32,'sym2','scal');
%     title('Scalogram') 
%     ylabel('Scale')
% 
%     %% Feature in time domain
%     TD(1)=sqrt(sum(series.^2)/length(series));
%     TD(2)=max(abs(series));
%     TD(3)=sum(series.^3)/(length(series)-1)./(TD(1).^3);
%     TD(4)=sum(series.^4)/(length(series)-1)./(TD(1).^4);
%     TD(5)=TD(2)/TD(1);
%     TD(6)=TD(2)*length(series)/sum(abs(series));
%     feature(sensororder,:)=TD;
% end

% %% Prediction by NN
% x=feature';
% % x=feature_transpose(1,:);
% t=ones(1,21)*121;
% net = fitnet(4);
% [net,tr] = train(net,x,t);
% view(net)
% y = net(x);
% perf = perform(net,t,y)
% nntraintool

    %% Feature in Frequency domain
    %% Feature in Wavelet domain
    
    
% %% health index
% figure,plot(ithealth(train_FD001(1:192,6:26)',5));