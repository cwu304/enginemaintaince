% ITHEALTH Information-theory based estimator of system health
%
% Usage: [healthout] = ithealth(inputmatrix, windowsize, sigavgs, sigstds)
%
% ITHEALTH is derived from the research code INFOHEALTH developed by
% Robert Sneddon, September 2009. Modifications made by Ryan Mackey.
%
% ITHEALTH provides an estimate of system health from performance data,
% updated with each new set of information. This information is assumed
% to be in rectangular form, i.e. N sensors by M samples, but may be
% irregular. Typically, this information is provided as an average of
% performance sensors that is updated only once per cycle, i.e. an average
% snapshot of performance for a single flight or a single operation of
% the system. This signal is assumed to have no dynamic information.
% Instead, trends in the value of the performance data from one run to the
% next may provide an indication of system health.
%
% The ITHEALTH algorithm estimates system health from local signal
% variances. If these variances are random and uncorrelated, then the
% information content of the signals will be very low. Conversely, a high
% information content indicates a pattern to the signal variances, i.e. an
% emerging trend. Since systems rarely trend towards getting healthier,
% this is in general evidence of a deteriorating condition. This can also
% be seen by a consideration of system stability -- when a system is
% healthy, it has a �dead band� of stability, where minor deviations in
% any particular signal do not affect the rest of the system. But as it
% ages and wears, its stability is compromised, or excursions finally pass
% beyond the �dead band,� and in so doing create a global response, which
% we detect as information in the signals.
%
% ITHEALTH accepts any number of signals. In testing this number is
% usually in the high single to low double digits. If the signal count is
% very low (say less than four) it will become more difficult to estimate
% the information content.
%
% The only parameter is the window size. This window defines how local
% our local variance is. The default value is 10, i.e. we will examine
% trends over the last 10 cycles compared to the entire history of
% operation. For small numbers of signals this value may be increased,
% which will introduce additional lag in the estimate, but may provide
% better accuracy.
%
% ITHEALTH also has better performance if the signal averages and standard
% deviations during normal operation are known ahead of time (for
% instance, the standard operating point and noise characteristics of the
% sensors). These parameters are generally available from specifications
% or can be computed from nominal data. If available, they can be
% supplied to ITHEALTH as vectors, each of length equal to the number of
% sensors. If not available, ITHEALTH will automatically estimate these
% values from the first few measurements supplied, i.e. the first
% windowful of data. It is not recommended to supply only the averages
% without also providing the standard deviations. However, the algorithm
% is not particularly sensitive to these parameters.
%
% ITHEALTH simulates �real time� operation by reading in the input
% file as a single matrix, then sending the contents line by line to the
% algorithm itself. For an embedded application, remove this shell and
% apply live data to the algorithm directly, while making provisions to
% retain past data for purposes of windowing.
%
% ITHEALTH (C) 2009, Jet Propulsion Laboratory
% Original Version 6 October 2009
% This Version 1.0, 6 October 2009
% Contact: Ryan Mackey, Ryan.M.Mackey@jpl.nasa.gov, (818) 354 9659
% ----------
function [healthout] = ithealth(inmat, windowsize, sigavgs, sigstds)
% Check for input parameters
if exist('windowsize') ~= 1 % If no windowsize provided,
    windowsize = 10; % Set default window size to 10
end
if exist('sigavgs') ~= 1 % If signal averages not provided, estimate from
    sigavgs = mean(inmat(:,1:windowsize),2); % first windowful of samples
end
if exist('sigstds') ~= 1 % If signal standard deviations not provided,
 sigstds = std(inmat(:,1:windowsize),0,2);% estimate these as well
end
% Initialize variables
[numsigs, numsamp] = size(inmat);
datawin = zeros(numsigs, windowsize);% Create buffer for windowed data
count = 0; % Initialize count of how many samples we've read
% Begin main loop
for i = 1 : numsamp
% Update data buffer
    for j = 1 : windowsize-1
        datawin(:,j) = datawin(:,j+1); % Shift window buffer by 1
    end
% Update window with current data
currentdata = inmat(:,i);
datawin(:,windowsize) = (currentdata-sigavgs) ./ sigstds;
% We normalize the incoming data by the average and std devs
count = count + 1; % Keep track of how many samples so far
% Calculate sum of individual variances
sumvar = 0;
for k = 1 : numsigs
sumvar = sumvar + var(datawin(k,:));
end
% Calculate overall variance for current window
tempwin = reshape(datawin,1, windowsize * numsigs);% Put whole window
%into a single vector
allvar = var(tempwin);     % and then compute the variance of this
%whole vector
% Now compute and store overall information measure
healthout(i) = (windowsize - 1) * sumvar / ((numsigs * windowsize - 1)*allvar);
% However, if window is not yet full, substitute a value of 1 since
% computation is invalid
if count < windowsize
    healthout(i) = 1;
end
end% End main loop