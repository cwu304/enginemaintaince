% %% Load data
% load train_FD001.txt
% load test_FD001.txt
% load RUL_FD001.txt
% 
% load train_FD002.txt
% load test_FD002.txt
% load RUL_FD002.txt
% 
% load train_FD003.txt
% load test_FD003.txt
% load RUL_FD003.txt
% 
% load train_FD004.txt
% load test_FD004.txt
% load RUL_FD004.txt
% 
% save train_FD001
% save test_FD001
% save RUL_FD001
% 
% save train_FD002
% save test_FD002
% save RUL_FD002
% 
% save train_FD003
% save test_FD003
% save RUL_FD003
% 
% save train_FD004
% save test_FD004
% save RUL_FD004


%%
close all

%%
aa=test_FD001(1:192,3:26)';
for i=1:24
% i=1;
    bb(i,:)=zscore(aa(i,:));
end
% 
% figure,imshow(bb,[]);

for ii=1:100
    a(ii)=max(find(train_FD001(:,1)==ii));
    if ii>1
       b(ii)=a(ii)-a(ii-1);
    end
end
b(1)=a(1);

diff=max(test_FD001)-min(test_FD001);

%%
sampleno=10;
X=zeros(300,24,50);

for i=1:sampleno
    if i==1
        X(1:a(i),1:21,i)=train_FD001(1:a(i),6:26);
    else
        X(1:b(i),1:21,i)=train_FD001(a(i-1)+1:a(i),6:26);
    end
    [m,n]=size(X);
    X_normc=X./repmat(max(X),[m,1]);
%     figure,st = controlchart(X_normc,'chart',{'xbar' 'r'});
end

%% Feature calculations
feature=zeros(21,6);
for sensororder=1:21

    series=X(1:b(1),sensororder,1);
    
%     %% fft
%     figure,
%     spectrogram(series',5);colorbar
%     subplot(2,1,1),plot(series);grid on;
%     subplot(2,1,2),plot(abs(fft(series,100)));grid on;

    %% wavelet
    len = length(series);
    figure,
    [cw1,sc] = cwt(series,1:32,'sym2','scal');
    title('Scalogram') 
    ylabel('Scale')

    %% Feature in time domain
    TD(1)=sqrt(sum(series.^2)/length(series));
    TD(2)=max(abs(series));
    TD(3)=sum(series.^3)/(length(series)-1)./(TD(1).^3);
    TD(4)=sum(series.^4)/(length(series)-1)./(TD(1).^4);
    TD(5)=TD(2)/TD(1);
    TD(6)=TD(2)*length(series)/sum(abs(series));
    feature(sensororder,:)=TD;
end

% %% Prediction by NN
% x=feature';
% % x=feature_transpose(1,:);
% t=ones(1,21)*121;
% net = fitnet(4);
% [net,tr] = train(net,x,t);
% view(net)
% y = net(x);
% perf = perform(net,t,y)
% nntraintool

    %% Feature in Frequency domain
    %% Feature in Wavelet domain
    
    
% %% health index
% figure,plot(ithealth(train_FD001(1:192,6:26)',5));