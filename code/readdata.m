% Read data from text into matrix
fileID = fopen('RUL_FD001.txt','r');
formatSpec = '%d %f';
sizeA = [1 Inf];
% Read the file data, filling output array, A, in column order. fscanf reuses the format, formatSpec, throughout the file.

A = fscanf(fileID,formatSpec,sizeA)
fclose(fileID);

fileID = fopen('train_FD001.txt','r');
formatSpec = '%d %f';
sizeB = [1 Inf];
% Read the file data, filling output array, A, in column order. fscanf reuses the format, formatSpec, throughout the file.

B = fscanf(fileID,formatSpec,sizeB);
fclose(fileID);