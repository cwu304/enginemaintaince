# README #

• Developed a prediction model based on an aircraft engine degradation simulation data set with machine learning such as artificial neural network, PCA, wavelet transform method in MatLab. 

• Predicted the distribution of the remaining useful lives and made a decision on maintenance time. Got a good test result.